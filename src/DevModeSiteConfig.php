<?php

namespace ArtDesign\DevMode;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;

/**
 * DevModeSiteConfig
 * Adds some fields to the SiteConfig for using Dev-Tools
 *
 *
 * @package silverstripe-devmode
 */

class DevModeSiteConfig extends DataExtension
{
    private static $db = [
        'DevMode' => 'Boolean'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab("Root.Main", array(
            new CheckboxField("DevMode", "Entwickler-Modus")
        ));
    }
}