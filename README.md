# SilverStripe DevMode

## Requirements

* SilverStripe 4.x
* Silverstripe SiteConfig
* Theme with Bootstrap 4.x or UIKit 3.x Framework installed

## Maintainers

* griess@art-design.de
* drueen@art-design.de

## Description

This plugin adds some useful tools to the frontend for a better developing the frontend built with Bootstrap or UiKit.

## Installation with [Composer](https://getcomposer.org/)

First add the Repository to your composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "git@bitbucket.org:art-und-design/silverstripe-devmode.git"
    }
],
```
Now you can install the package.

```
composer require "art-und-design/silverstripe-devmode"
```
You can use an include now to use the tool.
```
<% include DevMode %>
```
Don't forget to dev/build!